﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;

public class Graph1 : MonoBehaviour
{
    [SerializeField]
    private GraphChart Graph;

    [SerializeField]
    private int TotalPoints = 10;

    [SerializeField]
    private float timeToUpdate = 1.0f;

    private float lastX = 0f;
    private float lastTime = 0f;

    private double t1, t1d;

    public float Capacity;

    MathModels Datos;

    void Start()
    {
        //Grafica nivel en el Tanque
        Graph.DataSource.AutomaticVerticallView = false;
        Graph.DataSource.VerticalViewSize = Capacity;

        if (Graph == null) // the ChartGraph info is obtained via the inspector
            return;
        float x = 3f * TotalPoints;
        Graph.DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        Graph.DataSource.ClearCategory("Graph1"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        Graph.DataSource.ClearCategory("Graph2"); // clear the "Player 2" category. this category is defined using the GraphChart inspector

        for (int i = 0; i < TotalPoints; i++)  //add random points to the graph
        {
            Graph.DataSource.AddPointToCategory("Graph1", System.DateTime.Now - System.TimeSpan.FromSeconds(x), 0.0f); // each time we call AddPointToCategory 
            Graph.DataSource.AddPointToCategory("Graph2", System.DateTime.Now - System.TimeSpan.FromSeconds(x), 0.0f); // each time we call AddPointToCategory 
            x -= Random.value * 3f;
            lastX = x;
        }

        Graph.DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself

        Datos = FindObjectOfType<MathModels>();
    }

    private void Update()
    {
        t1 = Datos.hT;
        t1d = Datos.Hd;

        float time = Time.time;
        if (lastTime + timeToUpdate < time)
        {
            lastTime = time;
            lastX += Random.value * 3f;

            Graph.DataSource.AddPointToCategoryRealtime("Graph1", System.DateTime.Now, t1, 1f); // each time we call AddPointToCategory 
            Graph.DataSource.AddPointToCategoryRealtime("Graph2", System.DateTime.Now, t1d, 1f); // each time we call AddPointToCategory
        }
    }
}