﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using TMPro;


public class MathModels : MonoBehaviour
{
    //************************************* LIBRERIAS PARA MEMORIA COMPARTIDA *********************************************
    const string dllPath = "smClient64.dll";

    [DllImport(dllPath)]
    static extern int openMemory(String nombre, int type);

    [DllImport(dllPath)]
    static extern float getFloat(String memnombre, int position);

    [DllImport(dllPath)]
    static extern void setInt(String memnombre, int position, int value);

    [DllImport(dllPath)]
    static extern void setFloat(String memnombre, int position, float value);
    //***********************************************************************************************************************
    //*********************************************** VARIABLES PARA EL PROCESO *********************************************

    public int k = 0;
    // Start is called before the first frame update
    public float time;

    public float D;             // Diámetro Tanque[m]
    public float V;             // Constante seccion transversal de la tuberia 2
    public float A;             // Area de transferencia de calor
    public float To;            // Tiempo de muestreo
    public float TimeSample;    // Referencia del tiempo de muestreo
    public float C;             // Descomposicion del flujo volumetrico
    public float Vj;            // Volumen chaqueta [m^3]
    public float U;             // Coeficiente de transferencia de calor total [KJ/m^2]
    public float Cv1;           // Apertura real valvula de paso [0-1]
    public float Cv2;           // Apertura de valvula 1 (CV - Variable controlada)
    public float DeltaP;        // Caida de presion valvula
    public float GSv;           //Gravedad especifica del vapor
    public float Cpa;           // Calor específico del agua [KJ/kgC]
    public float Cpv;           // Calor específico del vapor [KJ/kgC]
    public float Ro;            //Densidad del agua [kg/m^3]
    public float Rov;           //Densidad del vapor [kg/m^3]
    public float Lamda;         //Calor latente de evaporacion a 90 grados
    public float Pe;            //Peso específico del fluido agua [kg/m^2 min]
    public float Aw;            //Celsius Antointe cte
    public float Bw;            //Antoine constante
    public float Cw;            //Antoine constante
    public float Ti;            //Temperatura inicial del fluido [°C]
    public float Tji;           //Temperatura inicial chaqueta [°C]
    public float Fine;
    public float Fjie;
    public float Fin;
    public float Fji;
    public float hDOT;          //Altura
    public float hTDOT;         //Temperatura
    public float TJDOT;         //Temperatura en la chaqueta
    public float h;             //Altura tanque
    public float T;             //Temperatura agua [°C]
    public float Tj;            //Temperatura inicial chaqueta [°C]
    public float hT;            //Altura*Temperatura
    public float TJT;           //TempCh*TempH2O
    public float delta;

    [SerializeField]
    private Vector3 position;    //declaracion de un vector para saber la posicion del recipiente
    private double percent;      //porcentaje de llenado que se encuentra el recipiente
    private double heigth;       //altura del volumen que esta recibiendo el recipiente
   
    [SerializeField]
    private double velocity; //es el valor con que velocidad se va realizar la animacion del llenado del recipiente
    [SerializeField]
    private double volume; //volumen del liquido que debe ir en el recipiente calculado;
    private double aux; //creacion de una variable para la ayuda del cambio del signo
    private float f; 
    private float hant; 
    private float hTant; 
    private float Tant; 
    private float TJTant;

    public float Hd, Td;
    public float rp1, rp2, rp3, rp4;

    //Animaciones
    public Transform Tanque;
    [SerializeField]
    private ChaquetColor Color = new ChaquetColor();

    public Slider sliderHd, sliderTd;
    public Slider sliderP1, sliderP2, sliderP3, sliderP4;
    public TextMeshProUGUI sp1T, sp2T;

    public TextMeshProUGUI P1, P2, P3, P4;

    //Scripts de animaciones
    //[SerializeField]
    //private RegadoEvap1 runRain1 = new RegadoEvap1();
    //[SerializeField]
    //private RegadoEvap2 runRain2 = new RegadoEvap2();

    //[SerializeField]
    //private ColorEvap Color = new ColorEvap();

    //***********************************************************************************************************************

    void Start()
    {

        openMemory("MemoriaUnity", 2);
        openMemory("MemoriaMatlab", 2);

        To = 0;   // [s]
        TimeSample = 0.01f; //[s] Tiempo de muestreo

        // DIMENSIONES DEL TANQUE
        D = 0.4064f;                  // Diámetro Tanque[m]
        V = 0.057f;                   // Volumen tanque
        C = (Mathf.PI * D * D) / 4;         // Descomposicion del flujo volumetrico

        // DIMENSIONES DE LA CHAQUETA
        Vj = 0.16f;                   // Volumen chaqueta[m ^ 3]
        U = 40.8526f;                 // Coeficiente de transferencia de calor total[KJ / m ^ 2]
        A = 0.83f;

        //COEFICIENTES ANTOINE DE PRESION
        Aw = 23.636f;
        Bw = 4169.84f;
        Cw = 244.485f;

        // VALVULAS DE CONTROL
        Cv1 = 2.54e-7f;               
        Cv2 = 0.78575f;               
        DeltaP = 10;

        //PROPIEDADES DEL LÍQUIDO(H2O)
        GSv = 0.35f;                  
        Cpa = 4.1851f;               
        Cpv = 1.99512f;               
        Ro = 1000f;                   
        Rov = 0.50484f;               
        Lamda = 2282;               
        Pe = 35.28e6f;                

        //Condiciones iniciales
        Ti = 20;                     
        Tji = 260;                                      
        Fine = 9.33e-4f;
        Fjie = 4.2f;

        hDOT = 0;               
        hTDOT = 0;               
        TJDOT = 0;
        h = 0.38f;                
        T = 20;                 
        Tj = 0;                
        hT = h * T;          
        TJT = Tj * T;
        delta = 0.01f;
        

    }

    // Update is called once per frame
    void Update()
    {
        ModeloChaquetaTermica();

    }

    private void ModeloChaquetaTermica()
    {
        To += Time.deltaTime; // Time.deltaTime = 1/fps, si 60 fps -> Time.deltaTime = 0.01666 [s] = 16.66 [ms] 
                              //time += Time.deltaTime;

        if (To >= TimeSample)
        {

            time += Time.deltaTime;


            //************ RECIBE DATOS DE MATLAB******************//
            Fin = getFloat("MemoriaMatlab", 0) + rp1; // Variable controlada - Apertura de V1 para que el error converja a cero
            Fji = getFloat("MemoriaMatlab", 1) + rp2; // Variable controlada - Apertura de V2 para que el error converja a cero

            Hd = sliderHd.value;
            Td = sliderTd.value;

            rp1 = sliderP1.value;
            rp2 = sliderP2.value;
            rp3 = sliderP3.value;
            rp4 = sliderP4.value;

            sp1T.text = Hd.ToString("F2");
            sp2T.text = Td.ToString("F2");
            P1.text = rp1.ToString("F2");
            P2.text = rp2.ToString("F2");
            P3.text = rp3.ToString("F2");
            P4.text = rp4.ToString("F2");



            //************ MODELO Chaqueta termica **************************

            hDOT = (Fin - Cv1 * Mathf.Sqrt(h * Pe)) / C;
            hTDOT = ((Fin * Ti - (Cv1 * T * Mathf.Sqrt(h * Pe))) + ((U * A * (Tj - T) )/ (Ro * Cpa)) )/ C;
            TJDOT = ((Fji * (Tji - Tj)) / Vj) - ((U * A * (Tj - T)) / (Vj * Rov * Cpv));

            //Integracion Euler
            hant = h;
            TJTant = TJT;
            hTant = hT;
            Tant = T;
            h = h + hDOT * 0.01f;
            hT = hT + hTDOT * 0.01f;
            TJT = TJT + TJDOT * 0.01f;
            T = hTant / hant;
            Tj = TJTant / Tant;
            
                       
            //************ ENVIA DATOS A MATLAB******************//
            setFloat("MemoriaUnity", 0, h);
            setFloat("MemoriaUnity", 1, hT + rp3);
            setFloat("MemoriaUnity", 2, TJT);
            setFloat("MemoriaUnity", 3, T + rp4);
            setFloat("MemoriaUnity", 4, Tj);

            setFloat("MemoriaUnity", 5,Hd);
            setFloat("MemoriaUnity", 6,Td);

            To = 0;
            Llenado();
            print(T);
        }
    }

    private void Llenado()
    {
        //Tanque 1
        aux = h;
        percent = (aux * 100) / 0.4;
        heigth = (percent * 0.0485f) / 100; //altura que debe tomar el objeto para realizar el llenado
        float scaleY = System.Convert.ToSingle(position.y + heigth * velocity); //calculado el valor
        Tanque.transform.localScale = new Vector3(Tanque.transform.localScale.x, scaleY, Tanque.transform.localScale.z);//colocando el vector de escala para la animacion del agua.
        float transformX = System.Convert.ToSingle(position.x + heigth * velocity);//calculado el valor
        Tanque.transform.localPosition = new Vector3(position.x, transformX, position.z);//colocando el vector de la posicion.

        //Color chaqueta
        f = Tj;
        Color.activerun3(f);
    }

}
