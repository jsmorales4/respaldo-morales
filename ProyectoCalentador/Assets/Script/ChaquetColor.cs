﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaquetColor : MonoBehaviour
{

    private bool esColor = false;
    private Renderer colorEvap;
    // Start is called before the first frame update
    void Start()
    {
        colorEvap = GetComponent<Renderer>();
        colorEvap.material.SetColor("_Color", Color.white);
        esColor = true;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void activerun3(float active)
    {
        print(active);
        if (active > 0f & active < 30f)
        {
            colorEvap.material.SetColor("_Color", Color.gray);
            esColor = false;
        }
        else if (active > 31f & active < 40f)
        {
            colorEvap.material.SetColor("_Color", new Color(0.8f, 0.6f, 0.7f));
            esColor = false;
        }
        else if (active > 41f & active < 45f)
        {
            colorEvap.material.SetColor("_Color", new Color(0.9f, 0.5f, 0.2f));
            esColor = false;
        }
        else if (active > 46f & active < 50f)
        {
            colorEvap.material.SetColor("_Color", new Color(0.9f, 0.4f, 0f));
            esColor = false;
        }
        else if (active > 51f & active < 55f)
        {
            colorEvap.material.SetColor("_Color", new Color(0.9333333f, 0.2105694f, 0.7450977f));
            esColor = false;
        }
        else if (active > 56f & active < 60f)
        {
            colorEvap.material.SetColor("_Color", new Color(0.9333333f, 0.1152771f, 0.07450977f));
            esColor = false;
        }
        else if (active > 61f & active < 80f)
        {
            colorEvap.material.SetColor("_Color", new Color(1f, 0.07f, 0.08f));
            esColor = false;
        }
        else if (active > 260f)
        {
            colorEvap.material.SetColor("_Color", Color.red);
            esColor = false;
        }
    }
}
