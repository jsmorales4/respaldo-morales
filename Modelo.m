function Valor = Modelo(Variables,h_,T_,Tj_,Fin_,Fji_,Vj,A,t,To)
Resultadosalida = 0;
DatosReales = [h_;T_;Tj_]';
%% DIMENSIONES DEL TANQUE 
D=0.4064;                  % Diámetro Tanque [m]
V=0.057;                   % Volumen tanque
C=(pi*D*D)/4;              % Descomposicion del flujo volumetrico
%%  Condiciones Iniciales
%% CONDICIONES INICIALES NO LINEAL
hDOT(1)=0.38;              % Altura
hTDOT(1)=0;               % Temperatura
TJDOT(1)=0;               % Temperatura en la chaqueta
h(1)=0.38;                % Altura inicial del fluido del tanque [m]
T(1)=20;                  % Temperatura agua [°C]
Tj(1)=260;                % Temperatura inicial chaqueta [°C]
hT(1)=h(1)*T(1);          % Altura*Temperatura
TJT(1)=Tj(1)*T(1);        % TempCh*TempH2O
 
for i=1:length(t)
    U_ = Variables(1);
    Cv1_ = Variables(2);
    Cpa_ = Variables(3);
    Cpv_ = Variables(4);
    Ro_ = Variables(5);
    Rov_ = Variables(6);
    Pe_ = Variables(7);
    Ti_ = Variables(8);
    Tji_ = Variables(9);
      
  %% MODELO NO LINEAL
hDOT(i)=(Fin_(i)-Cv1_*(h(i)*Pe_)^0.5)/C;
hTDOT(i)=(Fin_(i)*Ti_-(Cv1_*T(i)*(h(i)*Pe_)^0.5))+(U_*A*((Tj(i)-T(i)))/(Ro_*Cpa_))/C;
TJDOT(i)=(((Fji_(i)))/Vj)*(Tji_-Tj(i))-(U_*A*(Tj(i)-T(i))/(Vj*Rov_*Cpv_));
%% METODO DE EULER
h(i+1)= (h(i)+hDOT(i)*To);
hT(i+1)=hT(i)+hTDOT(i)*To;
TJT(i+1)=TJT(i)+TJDOT(i)*To;
T(i+1)=hT(i)/h(i);
Tj(i+1)=TJT(i)/T(i);
    DatosModelo = [h(i);T(i);Tj(i)];
    Error = -DatosModelo+DatosReales(i,:)';
    Resultadosalida = (Error'*Error)+Resultadosalida;
end
Valor = (Resultadosalida)/2*length(t);
end