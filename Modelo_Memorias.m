
                             %% UNIVERSIDAD DE LAS FUERZAS ARMADAS ESPE SEDE LATACUNGA
                             % PROYECTO UNIDAD 2: Memorias Compartidas
                             % TANQUE CONTINUAMENTE AGITADO PRESURIZADO CON INTERCAMBIADOR DE CALOR 
                             % APLICACI�N EN LA INDUSTRIA


%% Limpiar
clear all
clc
clear all
close all
warning off
%% Datos de simulacion
tfin=200;
ts=0.1; %TIEMPO DE MUESTREO
t=[0:ts:tfin];  

%% DIMENSIONES DEL TANQUE 
D=0.4064;                  % Diámetro Tanque [m]
V=0.057;                   % Volumen tanque
C=(pi*D*D)/4;              % Descomposicion del flujo volumetrico

%% DIMENSIONES DE LA CHAQUETA
Vj=0.16;                   % Volumen chaqueta [m^3]
U=40.8526;                 % Coeficiente de transferencia de calor total [KJ/m^2]
A=0.83;                    % Area de transferencia de calor [m^2]

%% VALVULAS DE CONTROL
Cv1=2.54e-7;               % Constante de descarga de la válvula
Cv2=0.78575;               % Constante de descarga de la válvula de vapor
DeltaP=10;                 % Caida de presion valvula

%% PROPIEDADES DEL L�?QUIDO (H2O)
GSv=0.35;                  % Gravedad especifica del vapor
Cpa=4.1851;                % Calor específico del agua [KJ/kgC]
Cpv=1.99512;               % Calor específico del vapor [KJ/kgC]
Ro=1000;                   % Densidad del agua [kg/m^3]
Rov=0.50484;               % Densidad del vapor [kg/m^3]
Lamda=2282;                % Calor latente de evaporacion a 90 grados
Pe=35.28e6;                % Peso específico del fluido agua [kg/m^2 min]

%% COEFICIENTES ANTOINE DE PRESION
                           % Parámetros empíricos, específicos de la sustancia
Aw=23.636;                 % Celsius Antointe cte
Bw=4169.84;                % Antoine constante
Cw=244.485;                % Antoine constante

%% CONDICIONES INICIALES
Ti=20;                     % Temperatura inicial del fluido [°C]
Tji=260;                   % Temperatura inicial chaqueta [°C]
To=92;                     % Temperatura final tanque [°C]
Fine=9.46e-4;              % Flujo de equilibrio de entrada de agua              
Fjie=3.91;                  % Flujo de equilibrio de vapor en la chaqueta

% Cargamos las librerias de la memoria compartida 
    loadlibrary('smClient64.dll','./smClient.h')
   
% Crear memorias
calllib('smClient64','createMemory','MemoriaUnity',10,2)   %Crear memoria "MemoriaUnity"
calllib('smClient64','createMemory','MemoriaMatlab',10,2)  %Crear memoria "MemoriaMatlab"
    
%Abrimos la memoria compartida 
    calllib('smClient64','openMemory','MemoriaUnity',2)
    calllib('smClient64','openMemory','MemoriaMatlab',2)
    
% %% Ponemos en cero todos los valores almacenados en la memoria
% for i=0:10
%     calllib('smClient64','setFloat','MemoriaUnity',i,0);
%     calllib('smClient64','setFloat','MemoriaMatlab',i,0);
% end

for k=1:length(t)
     tic
     %% Valores enviados desde Unity
        h(k) = calllib('smClient64','getFloat','MemoriaUnity',0);
        T(k) = calllib('smClient64','getFloat','MemoriaUnity',1);
        Tj(k) = calllib('smClient64','getFloat','MemoriaUnity',2);
        hd(k) = calllib('smClient64','getFloat','MemoriaUnity',3)
        Td(k) = calllib('smClient64','getFloat','MemoriaUnity',4);
        
     %% Errore de control
        he(k) = hd(k)-h(k);
        Te(k) = Td(k)-T(k);
        error = [he(k) Te(k)]';
     %% Proceso
        A1 = [Ti/C 0;...
               0  (Tji-Tj(k))/Vj]
         b11 = ((-(Cv1*T(k)*(h(k)*Pe)^0.5))+(U*A*((Tj(k)-T(k))))/(Ro*Cpa))/C;
         b21 = (-(U*A*(Tj(k)-T(k))))/(Vj*Rov*Cpv);
        Br = [b11;b21];
                  %% Matriz de ganancia
        W = [1 0;0 1];
     %% Ley de control
         uref = inv(A1)*W*tanh(error) - inv(A1)*Br;
         Fin(k) = uref(1);
         Fji(k) = uref(2);
     %% Valores enviados a Unity
        calllib('smClient64','setFloat','MemoriaMatlab',0,Fin(k));
        calllib('smClient64','setFloat','MemoriaMatlab',1,Fji(k));
     %% Tiempo de muestro
        while (toc < ts)
        end
        dt(k)= toc; 

end
 
%% Gr�ficas con datos de UNITY

figure(1)
subplot(3,1,1)
plot(t,h(1:length(t)),'r','LineWidth',2);
title('Altura del Fluido en el Tanque')
xlabel('Tiempo[s]')
ylabel('Altura [m] ')
grid on

subplot(3,1,2)
plot(t,T(1:length(t)),'r','LineWidth',2);
title('Temperatura del Tanque')
xlabel('Tiempo[s]')
ylabel('Temperatura [�C] ')
grid on

subplot(3,1,3)
plot(t,Tj(1:length(t)),'b','LineWidth',2);
title('Temperatura de la Chaqueta ')
xlabel('Tiempo[s]')
ylabel('Temperatura [�C] ')
grid on

% legend({'N'},'Location','southeast')

calllib('smClient64','freeViews')
unloadlibrary smClient64
