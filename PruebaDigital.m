                             %% UNIVERSIDAD DE LAS FUERZAS ARMADAS ESPE SEDE LATACUNGA
                             % PROYECTO UNIDAD 3: Controlador de la planta
                             % TANQUE CONTINUAMENTE AGITADO PRESURIZADO CON INTERCAMBIADOR DE CALOR 
                             % APLICACI�N EN LA INDUSTRIA

%% Limpiar
clear all
clc
clear all
close all
warning off

%% DIMENSIONES DEL TANQUE 
D=0.4064;                  % Diámetro Tanque [m]
V=0.057;                   % Volumen tanque
C=(pi*D*D)/4;              % Descomposicion del flujo volumetrico

%% DIMENSIONES DE LA CHAQUETA
Vj=0.16;                   % Volumen chaqueta [m^3]
U=40.8526;                 % Coeficiente de transferencia de calor total [KJ/m^2]
A=0.83;                    % Area de transferencia de calor [m^2]

%% VALVULAS DE CONTROL
Cv1=2.54e-7;               % Constante de descarga de la válvula
Cv2=0.78575;               % Constante de descarga de la válvula de vapor
DeltaP=10;                 % Caida de presion valvula

%% PROPIEDADES DEL L�?QUIDO (H2O)
GSv=0.35;                  % Gravedad especifica del vapor
Cpa=4.1851;                % Calor específico del agua [KJ/kgC]
Cpv=1.99512;               % Calor específico del vapor [KJ/kgC]
Ro=1000;                   % Densidad del agua [kg/m^3]
Rov=0.50484;               % Densidad del vapor [kg/m^3]
Lamda=2282;                % Calor latente de evaporacion a 90 grados
Pe=35.28e6;                % Peso específico del fluido agua [kg/m^2 min]

%% CONDICIONES INICIALES
Ti=20;                     % Temperatura inicial del fluido [°C]
Tji=260;                   % Temperatura inicial chaqueta [°C]

%% CONDICIONES INICIALES NO LINEAL
hDOT(1)=0;                % Altura
hTDOT(1)=0;               % Temperatura
TJDOT(1)=0;               % Temperatura en la chaqueta
h(1)=0.20;                % Altura inicial del fluido del tanque [m]
T(1)=20;                  % Temperatura agua [°C]
Tj(1)=0;                  % Temperatura inicial chaqueta [°C]
hT(1)=h(1)*T(1);          % Altura*Temperatura
TJT(1)=Tj(1)*T(1);        % TempCh*TempH2O



%% TIEMPOS DE SIMULACIÓN
delta=0.01;
tend = 1000;
t=[0:delta:tend];
% hd = 1.45*ones(1,length(t));
% Td = 25*ones(1,length(t));

hd = 2*[0.9*ones(1,20000) 0.5*ones(1,20000) 0.8*ones(1,20000) 0.4*ones(1,20000) 0.7*ones(1,length(t)-80000)];
Td= 40*[0.9*ones(1,50000) 0.6*ones(1,50000) 0.7*ones(1,length(t)-100000)];

%% MODELOS IMPLEMENTADOS
for k=1:length(t)
    
     %% Errore de control
        he(k) = hd(k)-hT(k);
        Te(k) = Td(k)-T(k);
        error = [he(k) Te(k)]';
        
        %% Proceso
        A1 = [Ti/C 0;...
               0  (Tji-Tj(k))/Vj];
         b11 = ((-(Cv1*T(k)*(h(k)*Pe)^0.5))+(U*A*((Tj(k)-T(k))))/(Ro*Cpa))/C;
         b21 = (-(U*A*(Tj(k)-T(k))))/(Vj*Rov*Cpv);
        Br = [b11;b21];
        %% Matriz de ganancia
        W = [1 0;0 10];
%         W = [1 0;0 1];
     %% Ley de control
         uref = inv(A1)*W*tanh(0.95*error) - inv(A1)*Br;
%          Fin(k) = uref(1);
         Fji(k) = uref(2);
         Fin(k) = min(1,max(0,(uref(1))));
%          Fji(k) = min(1,max(0,(uref(2))));
    
    
    %% MODELO NO LINEAL
   hDOT(k)=(Fin(k)-Cv1*(h(k)*Pe)^0.5)/C;  
   hTDOT(k)=(Fin(k)*Ti-(Cv1*T(k)*(h(k)*Pe)^0.5))+(U*A*((Tj(k)-T(k)))/(Ro*Cpa))/C;
   TJDOT(k)=(Fji(k)*(Tji-Tj(k)))/Vj -(U*A*(Tj(k)-T(k)))/(Vj*Rov*Cpv); 
   
%% METODO DE EULER
   h(k+1)=h(k)+hDOT(k)*delta;
   hT(k+1)=hT(k)+hTDOT(k)*delta;
   TJT(k+1)=TJT(k)+TJDOT(k)*delta; 
   T(k+1)=hT(k)/h(k);
   Tj(k+1)=TJT(k)/T(k);

      end


%%  GRAFICAS
figure(1)
%ALTURA TANQUE
subplot(2,1,1)
plot(t,hT(1:length(t)),'b','LineWidth',2);hold on; grid on;
plot(t,hd(1:length(t)),'--m','LineWidth',2);hold on; grid on;
title('Altura del Fluido en el Tanque')
legend('T', 'hd');
xlabel('Tiempo[minutos]')
ylabel('Altura[m] ')

%TEMPERATURA EN EL TANQUE
subplot(2,1,2)
plot(t,T(1:length(t)),'r','LineWidth',2);hold on; grid on;
plot(t,Td(1:length(t)),'--g','LineWidth',2);hold on; grid on;
title('Temperatura del Tanque')
legend('T', 'Td')
xlabel('Tiempo[minutos]')
ylabel('Temperatura [�C] ')
grid on

%% FLUJOS DE ENTRADA
figure(2)
subplot(2,1,1)
plot(t,Fin(1:length(t)),'--b','LineWidth',1.5);hold on; grid on;
title('CV FLUJO DE AGUA DE ENTRADA ')
ylabel('Apertura m^3')
xlabel('Tiempo[s]')
subplot(2,1,2)
plot(t,Fji(1:length(t)),'--r','LineWidth',1.5);hold on; grid on;
ylim([0 1])
title('CV FLUJO DE VAPOR DE ENTRADA')
ylabel('Apertura m^3 ')
xlabel('Tiempo[s]')
grid on
figure(3)
subplot(2,1,1)
plot(t,he(1:length(t)),'b','LineWidth',2);hold on; grid on;
title('Error de Nivel ')
ylabel('m')
xlabel('Tiempo[s]')
subplot(2,1,2)
plot(t,Te(1:length(t)),'--m','LineWidth',2);hold on; grid on;
title('Error de temperatura')
ylabel('�C ')
xlabel('Tiempo[s]')
